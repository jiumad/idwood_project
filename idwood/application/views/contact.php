<div id="content" style="">
		<section class="container">
			<div class="contact-title2">
			<h1>اطلاعات تماس</h1>
			</div>
			<section class="row address">
				<section class="col col-lg-4 col-md-4 col-sm-12 col-xs-12 address">
					<span class="glyphicon glyphicon-map-marker"></span>
					<h2 class="contact-h2">آدرس :</h2>
				</section>
				<p class="col col-lg-8 col-md-8 col-sm-12 col-xs-12">
						دماوند - گیلاوند - بلوار شاهر- شهرک شاهد - کوچه یاس ۴

				</p>
			</section>

			<section class="row address">
				<section class="col col-lg-4 col-md-4 col-sm-12 col-xs-12 address">
					<span class="glyphicon glyphicon-earphone"></span>
					<h2 class="contact-h2">تلفن :</h2>
				</section>
				<p class="col col-lg-8 col-md-8 col-sm-12 col-xs-12">
						0912-6007891
				</p>
			</section>

			<section class="row address">
				<section class="col col-lg-4 col-md-4 col-sm-12 col-xs-12 address">
					<span class="glyphicon glyphicon-envelope"></span>
					<h2 class="contact-h2">ایمیل :</h2>
				</section>
				<p class="col col-lg-8 col-md-8 col-sm-12 col-xs-12">
						Reza@gmail.com

				</p>
			</section>
		
		</section>
		<section class="contact-form">
		<section class="container-fluid" style="">
		<div class="contact-title2">
			<h2 >می توانید اط ظریق فرم زیر هم به ما پیام بدهید</h2>
		</div>
		<div class="">
			<?php
				echo form_open('Contact/send_message');
			?>

			<div class="form-group col col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?php
									$data = array(
										'name'     => 'email',
								        'type'          => 'email',
								        'class'            => 'form-control col input-lg',
								        'placeholder'         => 'ایمیل',
								        'required'   => 'required'
									);	
									echo form_input($data);	
								?>

			</div>
			<div class="form-group col col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?php
									$data = array(
										'name'     => 'name',
								        'type'          => 'text',
								        'class'            => 'form-control col input-lg',
								        'placeholder'         => 'نام',
								        'required'   => 'required'
									);	
									echo form_input($data);	
								?>
			</div>
			
			
			<div class="form-group col col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php
									$data = array(
										'name'     => 'msg',
								        'class'            => 'form-control  input-lg',
								        'placeholder'         => 'پیام',
								        'rows'         => '7',
								        'required'   => 'required'
									);	
									echo form_textarea($data);	


								?>
				
			</div>

			<div class="form-group col col-lg-6 col-md-6 col-sm-6 col-xs-6">
				
				
			</div>
			<div class="form-group col col-lg-3 col-md-3 col-sm-3 col-xs-6">
				
			</div>
			
			<div class="form-group col col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<?php
									$data = array(
										'name'     => 'msg_sub',
								        'class'            => 'btn btn-lg btn-block contact-btn',
								        'value'         => 'ارسال پیام'
									);	
									echo form_submit($data);	
								?>
				
			</div>
			<?php echo form_close(); ?>
		</div>
		</section>
		</section>
	</div>