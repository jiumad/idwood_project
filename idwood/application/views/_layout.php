<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="<?php echo base_url();?>attaches/css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url();?>attaches/css/bootstrap-rtl.css">
		<link rel="stylesheet" href="<?php echo base_url();?>attaches/css/bootstrap-flipped.css">
		<link rel="stylesheet" href="<?php echo base_url();?>attaches/css/style.css">


		<title>idwood</title>
	</head>

	<body style="">
		
	<header class="" style="">
		<nav class="navbar navbar-default">
		  <div class="container">
		    <div class="navbar-header">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                        
      		</button>
		      <a class="navbar-brand" href="<?php echo base_url();?>index.php">IDWOOD</a>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
			    <ul class="nav navbar-nav">
			     <li class="active"><a href="<?php echo base_url();?>index.php">صفحه اصلی</a></li>
			      <li><a href="<?php echo base_url();?>index.php/works">کارها</a></li>
			      <li><a href="<?php echo base_url();?>index.php/contact">ارتباط با ما</a></li>
			      
			    </ul>
			</div>
		  </div>
		</nav>
	</header>
		
<?php echo $contents; ?>

	<footer class="">
		
	
		<section class="" id="social-sec">
			<h4 style="">ما را در شبکه های اجتماعی دنبال کنید</h4>
			<a class="social-a" href="#" target="_blank"><img class="social" src="<?php echo base_url();?>attaches/images/social/facebook.png"></a>
			<a class="social-a" href="#" target="_blank"><img class="social" src="<?php echo base_url();?>attaches/images/social/telegram.png"></a>
			<a class="social-a" href="#" target="_blank"><img class="social" src="<?php echo base_url();?>attaches/images/social/instagram.png"></a>
		</section>

		
	</footer>
	
		
	<script type="text/javascript" src="<?php echo base_url();?>attaches/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>attaches/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>attaches/js/respond.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>attaches/js/html5shiv.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>attaches/js/holder.js"></script>
	</body>
</html>
