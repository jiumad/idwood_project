<?php
/**
* 
*/
class Post_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function getAll()
	{
		$posts = $this->db->get('works');
		if($posts->num_rows())
		{
			$posts = $posts->result_array();
			foreach ($posts as $key=> $value) 
			{
				$id = $posts[$key]['id'];
				$query = $this->db->query("SELECT file_name FROM spc_images WHERE work_id=$id");
				$imgs = array();
				foreach ($query->result_array() as $row)
				{
					array_push($imgs,$row['file_name']);
		        	
				}
				$posts[$key]['img'] = $imgs;
			}
			//echo"<pre>";
			//print_r($posts);
		return $posts;
		}
	}
}


?>