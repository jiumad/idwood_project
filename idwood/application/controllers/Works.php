<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Works extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		
	}

	public function index()
	{
		$this->load->model('post_model');
		$data['posts']=$this->post_model->getAll();
		$this->template->load('works',$data);
	}
}

?>