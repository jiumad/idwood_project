<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		
	}

	public function index()
	{
		$this->template->load('contact');
	}
	public function fg()
	{
		$this->load->model('Message');
		$mes = $this->Message->f();
		echo "<pre>";
		echo print_r($mes);
	}
	public function send_message()
	{
		$message = array(
						'email'  => $this->input->post('email'),
						'name'  => $this->input->post('name'),
						'msg'  => $this->input->post('msg')

					);
		
		if(null !== $this->input->post('msg_sub')){
			
			$this->load->model('Message');
			$result = $this->Message->send($message);
			$data['result'] = $result;
			$data['status'] = true;
			$this->template->load('msg_status',$data);
		}
		
		
	}
}

?>