<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends APP_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		
	}

	public function index()
	{
		
		$this->load->model('message_m');
		$data['allmessages'] = $this->message_m->fetch(); 
		$this->template->load('messages',$data);
		
	}

	
}

?>