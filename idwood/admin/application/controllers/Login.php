<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		
	}

	public function index()
	{
		$this->load->view('login');
	}

	function auth()
	{
		$this->load->library('form_validation');

		$rules = array(
			array(
				'field'=>'username',
				'label'=>'Username',
				'rules'=>'required'
			),
			array(
				'field'=>'password',
				'label'=>'Password',
				'rules'=>'required'
			)
		);

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run()==FALSE)
		{
			$data['empty'] = TRUE;
			$this->load->view('login',$data);
		}else{
			$this->load->model('login_m');
			$this->login_m->check();
		}
	}
	
}

?>