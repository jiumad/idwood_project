<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends APP_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		
	}

	public function index()
	{
		
		$this->template->load('new_post');
		
	}

	function randomString($length = 25) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		$max = count($characters) - 1;
		for ($j = 0; $j < $length; $j++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}

	function add()
	{
		
		$this->load->library('form_validation');
		$rules = array(
			array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'required'
			),
			array(
				'field' => 'description',
				'label' => 'Description',
				'rules' => 'required'
			)

		);
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()==FALSE)
		{
			$data['empty']=TRUE;
			$this->template->load('new_post',$data);

		}else{
			$post_data['title'] = $this->input->post('title');
			$post_data['description'] = $this->input->post('description');
			
			$this->load->model('post_m');
			$ins_id = $this->post_m->insert($post_data);
			

			$filesCount = count($_FILES['userFiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
            	$filename =  str_replace("_", "", $_FILES['userFiles']['name'][$i]);
            	$filename =  str_replace(" ", "", $filename);
                $_FILES['userFile']['name'] = $this->randomString().$filename;
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

				$config['upload_path']          = '/var/www/html/idwood/attaches/images/post';
                $config['allowed_types']        = 'jpg|png';
                $config['max_size']             = 2000;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile'))
                {	
                	$zdata = array('upload_data' => $this->upload->data()); // get data
					$zfile = $zdata['upload_data']['full_path']; // get file path
					chmod($zfile,0777); // CHMOD file

                	$img = array(
                		'file_name'=> $_FILES['userFile']['name'],
                		'work_id'  => $ins_id
                	);
                	$this->post_m->img_ins($img);
                	$img_up = 1;
                }else{
                	echo "آپلود عکس با مشکل مواجه شد . لطفا از ابتدا شروع کنید";
                	echo $this->upload->display_errors();
                }
            }
            

           
			

            
		
			
				//redirect('works');
			
		}
	}
	
}

?>