<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Works extends APP_Controller {

	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		
	}

	public function index()
	{
		$this->load->model('post_m');
		$data['posts']=$this->post_m->getAll();
		//echo "<pre>";
		//print_r($data['posts']);
		$this->template->load('index',$data);
		
		
	}

	
}

?>