<?php
class Message_m extends CI_Model{

	function __construct(){
		parent::__construct();
		
	}

	public function fetch()
	{
		$query = $this->db->get('message');
		if($query->num_rows() > 0){
			return $query->result();
		}
		return array();
	}

	
}

?>