
	<section class="container" style="margin-top: 4em;">
		<?php
						echo form_open_multipart('post/add');
		?>
		  <div class="form-group">
		    <?php
									$data = array(
										'name'     => 'title',
								        'type'          => 'text',
								        'class'            => 'form-control input-lg',
								        'placeholder'         => 'عنوان محصول',
								        'required'   => 'required'
									);	
									echo form_input($data);	
			?>
		    
		  </div>
		  <div class="form-group">
		    <?php
									$data = array(
										'name'     => 'description',
								        'class'            => 'form-control input-lg',
								        'placeholder'         => 'توضیحات محصول',
								        'rows'   => '8',
								        'required' => 'required'

								   
									);	
									echo form_textarea($data);	
			?>
		   
		  </div>
		  <div class="form-group">

					<?php
								$data = array(
									'name'  => 'userFiles[]',
									'multiple' => 'multiple',
									'required' => 'required'
								);

								 echo form_upload($data); 

								 if(isset($empty) && $empty==TRUE)
								 {
								 	echo "پر کردن تمام فیلد ها الزامی است!";
								 }
					?> 
								
		  	
		  </div>
		  <div class="form-group">
		  	<?php
									$data = array(
										'name'     => 'post_sub',
								        'class'            => 'btn btn-default input-lg',
								        'value'         => 'ثبت پست'
									);	
									echo form_submit($data);	
				?>
		    
		  </div>
		<?php
			echo form_close();

		?> 



	</section>
		