<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>صنایع چوبی دماوند</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">

	<!-- <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'> -->
	
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>attaches/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>attaches/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>attaches/css/bootstrap.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>attaches/css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>attaches/css/owl.theme.default.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>attaches/css/style.css">


	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>attaches/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<header>
		<div class="container text-center">
			<div class="fh5co-navbar-brand">
				<a class="fh5co-logo" href="<?php echo base_url(); ?>">IDWOOD</a>
			</div>

			<div>
				<?php
					echo form_open('login/auth');

					$attr = array(
						'name'=>'username',
						//'required'=>'required',
						'placeholder'=>'username'
						);
					echo form_input($attr);
					echo "<br/>";											
					$attr = array(
						'name'=>'password',
						//'required'=>'required',
						'placeholder'=>'password'
						);
					echo form_password($attr);

					echo "<br/>";		
					if(isset($empty) && $empty == TRUE)
					{
						echo "<p class='alert alert-danger' style='color:red'>نام کاربری و رمز عبور الزامی است!</p>";
					}							
					$attr = array(
						'name'=>'login_sub',
						'value'=>'Login'
						);
					echo form_submit($attr);													
					echo form_close();
				?>

				
			</div>
		</div>
	</header>